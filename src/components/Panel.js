import React, { useEffect } from 'react';

function Panel(props) {
	useEffect(() => {
		props.setWord('Panel sudah terbuka');
		return () => {
			props.setWord('Klik tombol dibawah untuk membuka component');
		};
	}, []);
	return (
		<div
			style={{
				backgroundColor: 'green',
				color: 'white',
				padding: '20px',
				border: '6px solid black',
				marginTop: '30px',
			}}
		>
			<h1>Halo ini berasal dari Panel Component Pertama</h1>
		</div>
	);
}

export default Panel;
