import React from 'react';

function Article(props) {
	return (
		<div style={{ border: '3px solid blue', padding: '10px 20px', margin: '10px' }}>
			<h1 style={{ textDecoration: 'underline' }}>{props.obj.title}</h1>
			<p>{props.obj.content}</p>
		</div>
	);
}

export default Article;
