import React from 'react';

function PanelSecond() {
	return (
		<div
			style={{ backgroundColor: 'red', color: 'white', padding: '20px', border: '6px solid black', marginTop: '30px' }}
		>
			<h1>Halo ini berasal dari Panel Component Kedua</h1>
		</div>
	);
}

export default PanelSecond;
