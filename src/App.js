import React, { useState } from 'react';
import './App.css';

const App = () => {
	const [input, setInput] = useState({
		nama_depan: '',
		nama_belakang: '',
		domisili: '',
	});

	const handleType = event => {
		setInput({ ...input, [event.target.name]: event.target.value });
	};

	const submitHandler = () => {
		console.log(input);
	};

	return (
		<div className="App">
			<h1>Latihan Form Binding</h1>
			<input onChange={handleType} type="text" placeholder="Nama Depan" name="nama_depan" />
			<br />
			<br />
			<input onChange={handleType} type="text" placeholder="Nama Belakang" name="nama_belakang" />
			<br />
			<br />
			<input onChange={handleType} type="text" placeholder="Domisili" name="domisili" />
			<div style={{ marginTop: '20px' }}>
				<button onClick={submitHandler} className="btn btn-minus">
					Submit
				</button>
			</div>
		</div>
	);
};
export default App;
